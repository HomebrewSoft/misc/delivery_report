{
    "name": "delivery_report",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "stock",
        "sale_stock",
    ],
    "data": [
        # reports
        "reports/stock_picking.xml",
    ],
}
